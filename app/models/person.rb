class Person < ActiveRecord::Base
  include Contact
  
  validates :first_name, presence: true
  validates :last_name, presence: true
  
  def to_s
    "#{last_name}, #{first_name}"
  end
end
