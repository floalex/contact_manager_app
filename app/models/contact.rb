module Contact
  extend ActiveSupport::Concern
  
  # Any code defined inside the included block will be run on the class when the module is included
  included do
    has_many :phone_numbers, as: :contact
    has_many :email_addresses, as: :contact
  end
  
  # Any methods defined in the ClassMethods submodule will be defined on the including class
  # module ClassMethods
  # end
end