== README

Jumpstart Lab: Contact Manager Exercise

This exercise includes following topics:

Testing with RSpec
Creating view templates with Haml and Sass
Building reusable view code with helpers and partials
Refactoring
Managing authentication and authorization
Server and client-side validations
Deployment and monitoring